function deadLine(arrTeam, arrBackLog, deadData) {
  let teamPower = arrTeam.reduce((a, b) => a + b);
  let taskPower = arrBackLog.reduce((a, b) => a + b);
  let daysHave = Math.floor(((new Date(deadData)) - Date.now()) / (3600 * 24 * 1000));
  let workDays = daysWithoutHollidays(daysHave);
  let needDays = taskPower / teamPower;
  let refModul = Math.abs(needDays - workDays);
  if (needDays < workDays) {
    return `Всі завдання будуть успішно виконані за ${Math.round(refModul)} днів до дедлайну ! `
  } else if (needDays > workDays) {
    return `Команді розробників доведеться витратити додатково ${Math.round(refModul * 8)} годин після дедлайну, щоб виконати всі завдання в беклогі !`
  }else{
    return 'Good job , team   will make their work in time!'
  };
};

function daysWithoutHollidays(days) {
  let res = 0;
  let nowDate = new Date();
  for (let i = 0; i < days; i++) {
    nowDate.setDate(nowDate.getDate() + 1);
    if (nowDate.getDay() > 0 && nowDate.getDay() < 6) {
      res += 1;
    }
  } return res;
};

let date = new Date(2019, 5, 30);
let arrTeam = [5, 6, 4, 7, 8, 4];
let arrTask = [108, 87, 86, 82, 99, 645, 34, 100];

console.log(deadLine(arrTeam, arrTask, date))